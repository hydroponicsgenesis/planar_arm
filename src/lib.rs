#![no_std]

use libm::{cos, sin, powf, sqrt, acos, atan2};

pub struct Angles {
  pub q1: f32,
  pub q2: f32,
  pub q3: f32,
}

pub struct Arm {
  l1: f32,
  l2: f32,
  l3: f32,
}

impl Arm {
  pub fn new(l1: f32, l2: f32, l3: f32) -> Arm {
    Arm { l1, l2, l3 }
  }

  pub fn calculate_angles(&self, x: f32, y: f32, end_effector: f32) -> Angles {
    let xw = x - self.l3 * cos(end_effector as f64) as f32;
    let yw = y - self.l3 * sin(end_effector as f64) as f32;
    let r = sqrt((powf(xw, 2.0) + powf(yw, 2.0)) as f64) as f32;
    let q2 = self.calculate_q2(r as f32);
    let gamma = acos(((powf(r, 2.0) + powf(self.l1, 2.0) - powf(self.l2, 2.0)) / (2.0 * r * self.l1)) as f64) as f32;
    let q1 = self.calculate_q1(xw, yw, gamma);
    let q3 = end_effector - q1 - q2;

    let angles = Arm::calculate_elbow_up_configuration(q1, q2, q3, gamma);

    Angles {
      q1: angles.q1,
      q2: angles.q2,
      q3: angles.q3,
    }
  }

  fn calculate_q2(&self, r: f32) -> f32 {
    let beta = acos(((powf(self.l1, 2.0) + powf(self.l2, 2.0) - powf(r, 2.0)) / (2.0 * self.l1 * self.l2)) as f64) as f32;
    core::f32::consts::PI - beta
  }

  fn calculate_q1(&self, xw: f32, yw: f32, gamma: f32) -> f32 {
    let alpha = atan2(yw as f64, xw as f64) as f32;
    alpha - gamma
  }

  fn calculate_elbow_up_configuration(q1: f32, q2: f32, q3: f32, gamma: f32) -> Angles {
    Angles {
      q1: q1 + 2.0 * gamma,
      q2: -q2,
      q3: q3 + 2.0 * (q2 - gamma),
    }
  }
}

#[cfg(test)]
mod tests {
  use assert_approx_eq::assert_approx_eq;
  use crate::{Arm, Angles};

  #[test]
  fn when_upright_angles_should_be_as_expected() {
    let l1 = 3.0;
    let l2 = 5.0;
    let l3 = 6.0;
    let arm = Arm::new(l1, l2, l3);
    let angles = arm.calculate_angles(0.0, l1 + l2 + l3 - 0.0000001, core::f32::consts::PI / 2.0);
    let expected = Angles { q1: core::f32::consts::PI / 2.0, q2: 0.0, q3: 0.0 };
    assert_approx_eq!(expected.q1, angles.q1, 0.05);
    assert_approx_eq!(expected.q2, angles.q2, 0.05);
    assert_approx_eq!(expected.q3, angles.q3, 0.05);
  }

  #[test]
  fn when_level_with_ground_angles_should_be_as_expected() {
    let l1 = 3.0;
    let l2 = 5.0;
    let l3 = 6.0;
    let arm = Arm::new(l1, l2, l3);
    let angles = arm.calculate_angles(l1 + l2 + l3 - 0.0000001, 0.0, 0.0);
    let expected = Angles { q1: 0.0, q2: 0.0, q3: 0.0 };
    assert_approx_eq!(expected.q1, angles.q1, 0.05);
    assert_approx_eq!(expected.q2, angles.q2, 0.05);
    assert_approx_eq!(expected.q3, angles.q3, 0.05);
  }

  #[test]
  fn this_should_give_correct_angles() {
    let l1 = 112.0;
    let l2 = 116.0;
    let l3 = 107.0;
    let arm = Arm::new(l1, l2, l3);
    let x = 140.0;
    let y = 190.0;
    let angles = arm.calculate_angles(x, y, 0.0);
    let expected = Angles { q1: 1.9255058765411377, q2: -1.0794320106506348, q3: -0.8460739850997925 };
    assert_approx_eq!(expected.q1, angles.q1, 0.05);
    assert_approx_eq!(expected.q2, angles.q2, 0.05);
    assert_approx_eq!(expected.q3, angles.q3, 0.05);
  }
}